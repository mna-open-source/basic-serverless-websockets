# Serverless + WebSocket

## Prerequisite Software

Before you can work with this project, you must install and configure the following products on your development machine:

- [curl](https://curl.se/)
- [Redis](https://redis.io/)
- [Git](http://git-scm.com) and/or the **GitHub app** (for [Mac](http://mac.github.com) or [Windows](http://windows.github.com))
- [Node.js](http://nodejs.org) - **Current version** _v12_
- [Serverless](https://www.serverless.com/framework/docs/getting-started/)
- [Yarn](https://yarnpkg.com/getting-started/install)

It is recommendable to install node via [NVM](https://github.com/nvm-sh/nvm)

## Getting the Sources

Clone this repository:

```shell
# Clone
> git clone git@gitlab.com:mna-open-source/basic-serverless-websockets.git


# Go to the sources directory:
> cd basic-serverless-websockets
```

## Installing NPM Modules

Next, install the JavaScript modules needed to build and test the app:

```shell
# Install project dependencies (package.json)
> yarn install
```

## Quick Start

How to run your local environment.

```shell
serverless offline start
```

## To test communications

Open the file [websocket.html](test/websocket.html) in the browser or follow the instruction below:

```shell
npm install wscat -g
wscat -c ws://localhost:3001
```

or 

```shell
curl --request POST --data 'H1' http://localhost:3000/dev/sendMessage
```
