'use strict';

const logger = require('pino')();

const { addConnection, deleteConnection, sendMessageToAllConnected } = require('./resources/websocket');

const successfulResponse = {
    statusCode: 200,
    body: 'everything is alright'
};

/**
 * Connect handler
 * @param {{requestContext: {eventType: string, connectionId: string}}} event
 * @param {{functionName: String}} context
 * @param callback
 */
module.exports.connectHandler = (event, context, callback) => {
    const child = logger.child({ scopeName: context.functionName, scopeType: 'handler', source: 'websocket' });
    child.info({ msg: `The function ${context.functionName} was called!`, event, });

    // Handle connection
    addConnection(event.requestContext.connectionId)
        .then(() => {
            callback(null, successfulResponse);
        })
        .catch(err => {
            callback(null, JSON.stringify(err));
        });
};

/**
 * Disconnect handler
 * @param {{requestContext: {eventType: string, connectionId: string}}} event
 * @param {{functionName: String}} context
 * @param callback
 */
module.exports.disconnectHandler = (event, context, callback) => {
    const child = logger.child({ scopeName: context.functionName, scopeType: 'handler', source: 'websocket' })
    child.info({ msg: `The function ${context.functionName} was called!`, event, });

    // Handle disconnection
    deleteConnection(event.requestContext.connectionId)
        .then(() => {
            callback(null, successfulResponse);
        })
        .catch(err => {
            callback(null, {
                statusCode: 500,
                body: 'Failed to connect: ' + JSON.stringify(err)
            });
        });
};

/**
 * Default handler
 * @param {Object} event
 * @param {{functionName: String}} context
 * @param callback
 */
module.exports.defaultHandler = (event, context, callback) => {
    const child = logger.child({ scopeName: context.functionName, scopeType: 'handler', source: 'websocket' })
    child.info({ msg: `The function ${context.functionName} was called!`, event, });

    sendMessageToAllConnected(event).then(() => {
        callback(null, successfulResponse);
    }).catch (err => {
        callback(null, JSON.stringify(err));
    });
};

/**
 * Send messages handler
 * @param {Object} event
 * @param {{functionName: String}} context
 * @param callback
 */
module.exports.sendMessageHandler = (event, context, callback) => {
    const child = logger.child({ scopeName: context.functionName, scopeType: 'handler', source: 'websocket' })
    child.info({ msg: `The function ${context.functionName} was called!`, event, });

    sendMessageToAllConnected(event).then(() => {
        callback(null, successfulResponse);
    }).catch (err => {
        callback(null, JSON.stringify(err));
    });
}
