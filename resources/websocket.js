'use strict';

const logger = require('pino')();
const AWS = require('aws-sdk');
const { redisClient } = require('./redis');

const { promisify } = require('util');
// const getAsync = promisify(redisClient.get).bind(redisClient);
const setAsync = promisify(redisClient.set).bind(redisClient);
const delAsync = promisify(redisClient.del).bind(redisClient);
const keysAsync = promisify(redisClient.keys).bind(redisClient);

const prefix = 'connectionId:';

/**
 * Add a new connection
 * @param {string} connectionId Connection ID
 */
const addConnection = connectionId => setAsync(prefix + connectionId, '1');

/**
 * Delete a connection
 * @param  {string} connectionId Connection ID
 */
const deleteConnection = connectionId => delAsync(prefix + connectionId);

/**
 * Get connection IDs
 */
const getConnectionIds = () => keysAsync(prefix + '*').then(items => items.map(item => item.replace(prefix, '')));

/**
 * Send message
 * @param {{body: string, requestContext: { domainName: string, stage: string }}} event Invocation event
 * @param {string} connectionId Connection ID
 */
const send = ({body}, connectionId) => {
    const functionName = 'send';
    const child = logger.child({ scopeName: functionName, scopeType: 'resources' })
    child.info({ msg: `The function ${functionName} was called!`, body, connectionId, });

    const apigwManagementApi = new AWS.ApiGatewayManagementApi({
        apiVersion: '2018-11-29',
        endpoint: 'http://localhost:3001',
    });

    const params = {
        ConnectionId: connectionId,
        Data: body,
    };

    return apigwManagementApi.postToConnection(params).promise();
};

/**
 * Sen message to all connections
 * @param event Event
 * @returns {Promise<*[]>}
 */
const sendMessageToAllConnected = (event) => {
    const functionName = 'sendMessageToAllConnected';
    const child = logger.child({ scopeName: 'functionName', scopeType: 'resources' })
    child.info({ msg: `The function ${functionName} was called!`, event, });

    return getConnectionIds().then(connectionIds => {
        return connectionIds.map(connectionId => send(event, connectionId));
    });
}

module.exports = {
    addConnection,
    deleteConnection,
    sendMessageToAllConnected,
}
