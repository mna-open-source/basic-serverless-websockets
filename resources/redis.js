// [redis[s]:]//[[user][:password@]][host][:port][/db-number][?db=db-number[&password=bar[&option=value]]]
const redisUri = 'redis://127.0.0.1:8888';

// Redis client library
const redis = require('redis');

// Client instance
const redisClient = redis.createClient(redisUri);

module.exports = {
    redisClient
}
